package com.storecodes.rs.util;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class AssertUtil {

    private AssertUtil() {
    }

    public static void assertEqualDates(SimpleDateFormat dateFormat, String formattedDate1, Date date2) {
        String formattedDate2 = dateFormat.format(date2);

        assertEquals(formattedDate1, formattedDate2);
    }

    public static void assertEqualDates(SimpleDateFormat dateFormat, java.util.Date date1, Date date2) {
        String formattedDate1 = dateFormat.format(date1);
        String formattedDate2 = dateFormat.format(date2);

        assertEquals(formattedDate1, formattedDate2);
    }
}
