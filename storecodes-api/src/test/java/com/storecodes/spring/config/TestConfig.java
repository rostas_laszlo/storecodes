package com.storecodes.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.storecodes.rs.api.CustomerDataService;
import com.storecodes.rs.api.StoreCodesService;
import com.storecodes.rs.api.StoreDetailService;
import com.storecodes.rs.impl.CustomerDataServiceImpl;
import com.storecodes.rs.impl.StoreCodesServiceImpl;
import com.storecodes.rs.impl.StoreDetailServiceImpl;

@Configuration
@ComponentScan("com.storecodes")
@Profile(TestConfig.DEV_PROFILE_NAME)
public class TestConfig {

    public final static String DEV_PROFILE_NAME = "dev";

    public TestConfig() {
    }

    @Bean
    public StoreCodesService storeCodesService() {
        return new StoreCodesServiceImpl();
    }

    @Bean
    public StoreDetailService storeDetailService() {
        return new StoreDetailServiceImpl();
    }

    @Bean
    public CustomerDataService customerDataService() {
        return new CustomerDataServiceImpl();
    }
}
