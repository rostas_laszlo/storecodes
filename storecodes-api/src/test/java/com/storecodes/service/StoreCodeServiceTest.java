package com.storecodes.service;

import static com.storecodes.rs.util.AssertUtil.assertEqualDates;
import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.storecodes.data.model.Code;
import com.storecodes.data.model.Customer;
import com.storecodes.data.model.Store;
import com.storecodes.service.model.AddStoreCode;
import com.storecodes.spring.config.HibernateTestConfiguration;
import com.storecodes.spring.config.TestConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class, HibernateTestConfiguration.class }, loader = AnnotationConfigContextLoader.class)
@ActiveProfiles(TestConfig.DEV_PROFILE_NAME)
public class StoreCodeServiceTest {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private StoreCodeService storeCodeService;

    public StoreCodeServiceTest() {
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void testInvalidInputParameters() {
        storeCodeService.getStoreCode(33, 120);
    }

    @Test()
    public void testGetStoreCode() {
        Code code = storeCodeService.getStoreCode(1, 1);
        Store store = code.getStore();
        Customer customer = code.getCustomer();
        
        assertEquals("ÁFÁ-s számla", code.getName());
        assertEquals("11254", code.getData());
        assertEquals("number", code.getType());
        assertEquals(1, code.getId());
        assertEqualDates(dateFormat, "2014-02-20", code.getToValid());
        assertEqualDates(dateFormat, "2012-01-20", code.getCreated());
        
        assertCustomerId1(customer);
        assertStoreId1(store);
    }
    
    @Test
    public void testAddCode() {
        AddStoreCode newCode = getValidAddStoreCode();
        
        int newId = storeCodeService.addStoreCode(newCode, 1);
        Code code = storeCodeService.getStoreCode(1, newId);
        Store store = code.getStore();
        Customer customer = code.getCustomer();
        
        assertEquals(newCode.getName(), code.getName());
        assertEquals(newCode.getData(), code.getData());
        assertEquals(newCode.getType(), code.getType());
        assertEquals(newId, code.getId());
        assertEqualDates(dateFormat, newCode.getToValid(), code.getToValid());
        assertEqualDates(dateFormat, newCode.getCreated(), code.getCreated());

        assertCustomerId1(customer);
        assertStoreId1(store);
    }

    private void assertStoreId1(Store store) {
        assertEquals(1, store.getId());
        assertEquals(20.7759506, store.getLatitudeGpsCoordinate(), 0);
        assertEquals(48.0760666, store.getLongitudeGpsCoordinate(), 0);
        assertEquals("Miskolc, Mésztelep u. 1/a, 3508", store.getAddress());
        assertEquals("Tesco-avas", store.getName());
    }

    private void assertCustomerId1(Customer customer) {
        assertEquals("uni-miskolc", customer.getName());
        assertEquals(1, customer.getId());
        assertEquals("", customer.getExternalId());
    }
    
    private AddStoreCode getValidAddStoreCode() {
        AddStoreCode newCode = new AddStoreCode();
        newCode.setCreated(new Date());
        newCode.setCustomerId(1);
        newCode.setData("TEST123");
        newCode.setName("ABC-1234");
        newCode.setStoreId(1);
        newCode.setToValid(new Date());
        newCode.setType("QR");

        return newCode;
    }
}
