package com.storecodes.rs.model;

import java.util.Date;

public class AddCodeRequest {

    private int storeId;

    private String type;

    private String name;

    private String data;

    private Date toValid;

    public AddCodeRequest() {
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Date getToValid() {
        return toValid;
    }

    public void setToValid(Date toValid) {
        this.toValid = toValid;
    }
}
