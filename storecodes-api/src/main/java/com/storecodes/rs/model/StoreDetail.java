package com.storecodes.rs.model;

public class StoreDetail {

    private int id;

    private String name;

    private double longitudeGpsCoordinate;

    private double latitudeGpsCoordinate;

    private String address;

    public StoreDetail() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLongitudeGpsCoordinate() {
        return longitudeGpsCoordinate;
    }

    public void setLongitudeGpsCoordinate(double longitudeGpsCoordinate) {
        this.longitudeGpsCoordinate = longitudeGpsCoordinate;
    }

    public double getLatitudeGpsCoordinate() {
        return latitudeGpsCoordinate;
    }

    public void setLatitudeGpsCoordinate(double latitudeGpsCoordinate) {
        this.latitudeGpsCoordinate = latitudeGpsCoordinate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
