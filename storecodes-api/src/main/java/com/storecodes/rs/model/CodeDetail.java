package com.storecodes.rs.model;

import java.sql.Date;

public class CodeDetail {

    private CustomerInfo customerInfo;

    private StoreDetail storeDetail;

    private String type;

    private String name;

    private String data;

    private Date toValid;

    private Date created;

    public CodeDetail() {
    }

    public CustomerInfo getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(CustomerInfo customerInfo) {
        this.customerInfo = customerInfo;
    }

    public StoreDetail getStoreDetail() {
        return storeDetail;
    }

    public void setStoreDetail(StoreDetail storeDetail) {
        this.storeDetail = storeDetail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Date getToValid() {
        return toValid;
    }

    public void setToValid(Date toValid) {
        this.toValid = toValid;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
