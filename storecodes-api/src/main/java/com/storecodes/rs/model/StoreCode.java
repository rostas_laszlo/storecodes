package com.storecodes.rs.model;

import com.wordnik.swagger.annotations.ApiModelProperty;

public class StoreCode {

    @ApiModelProperty(required = true)
    private int id;

    @ApiModelProperty(required = true)
    private String name;

    @ApiModelProperty(required = true)
    private String type;

    @ApiModelProperty(required = true)
    private String storeName;

    @ApiModelProperty(required = true)
    private int storeId;

    @ApiModelProperty(required = true)
    private String customerName;

    @ApiModelProperty(required = true)
    private int customerId;

    @ApiModelProperty(required = true)
    private String data;

    public StoreCode() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
