package com.storecodes.rs.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.storecodes.data.model.Code;
import com.storecodes.data.model.Customer;
import com.storecodes.data.model.Store;
import com.storecodes.rs.model.AddCodeRequest;
import com.storecodes.rs.model.CodeDetail;
import com.storecodes.rs.model.CustomerInfo;
import com.storecodes.rs.model.StoreCode;
import com.storecodes.rs.model.StoreDetail;
import com.storecodes.service.model.AddStoreCode;

/**
* Static Util class Rest model converting
*
*/
public final class ModelConverter {

    private ModelConverter() {
    }

    /**
    * Convert Service to RS model
    *
    */
    public static Collection<StoreCode> convertToStoreCode(List<Code> codes) {
        Collection<StoreCode> storeCodes = new ArrayList<>();

        for (Code code : codes) {
            StoreCode storeCode = convertToStoreCode(code);

            storeCodes.add(storeCode);
        }

        return storeCodes;
    }

    /**
    * Convert Service to RS model
    *
    */
    public static List<CustomerInfo> convertToCustomerInfo(List<Customer> customers) {
        List<CustomerInfo> customerInfos = new ArrayList<>();

        for (Customer customer : customers) {
            CustomerInfo customerInfo = convert(customer);

            customerInfos.add(customerInfo);
        }

        return customerInfos;
    }

    /**
    * Convert Service to RS model
    *
    */
    public static List<StoreDetail> convertToStoreDetail(Collection<Store> stores) {
        List<StoreDetail> storeDetails = new ArrayList<>();

        for (Store store : stores) {
            StoreDetail storeDetail = convert(store);

            storeDetails.add(storeDetail);
        }

        return storeDetails;
    }

    /**
    * Convert Service to RS model
    *
    */
    public static StoreDetail convert(Store store) {
        StoreDetail storeDetail = new StoreDetail();
        storeDetail.setAddress(store.getAddress());
        storeDetail.setId(store.getId());
        storeDetail.setLatitudeGpsCoordinate(store.getLatitudeGpsCoordinate());
        storeDetail.setLongitudeGpsCoordinate(store.getLongitudeGpsCoordinate());
        storeDetail.setName(store.getName());

        return storeDetail;
    }

    /**
    * Convert Service to RS model
    *
    */
    public static CustomerInfo convert(Customer customer) {
        CustomerInfo customerInfo = new CustomerInfo();
        customerInfo.setExternalId(customer.getExternalId());
        customerInfo.setId(customer.getId());
        customerInfo.setName(customer.getName());

        return customerInfo;
    }

    /**
    * Convert Service to RS model
    *
    */
    public static CodeDetail convertToCodeDetail(Code storeCode) {
        CustomerInfo customerInfo = ModelConverter.convert(storeCode.getCustomer());
        StoreDetail storeDetail = ModelConverter.convert(storeCode.getStore());

        CodeDetail detail = new CodeDetail();
        detail.setCreated(storeCode.getCreated());
        detail.setCustomerInfo(customerInfo);
        detail.setData(storeCode.getData());
        detail.setName(storeCode.getName());
        detail.setStoreDetail(storeDetail);
        detail.setToValid(storeCode.getToValid());
        detail.setType(storeCode.getType());

        return detail;
    }

    /**
    * Convert Service to RS model
    *
    */
    public static StoreCode convertToStoreCode(Code code) {
        StoreCode storeCode = new StoreCode();
        storeCode.setName(code.getName());
        storeCode.setStoreName(code.getStore().getName());
        storeCode.setType(code.getType());
        storeCode.setStoreId(code.getStore().getId());
        storeCode.setId(code.getId());
        storeCode.setCustomerName(code.getCustomer().getName());
        storeCode.setCustomerId(code.getCustomer().getId());
        storeCode.setData(code.getData());

        return storeCode;
    }

    /**
    * Convert Service to RS model
    *
    */
    public static AddStoreCode convertToAddStoreCode(int customerId, AddCodeRequest code) {
        AddStoreCode newCode = new AddStoreCode();
        newCode.setCreated(new Date());
        newCode.setCustomerId(customerId);
        newCode.setData(code.getData());
        newCode.setName(code.getName());
        newCode.setStoreId(code.getStoreId());
        newCode.setToValid(code.getToValid());
        newCode.setType(code.getType());

        return newCode;
    }
}
