package com.storecodes.rs.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.storecodes.data.model.Store;
import com.storecodes.rs.api.StoreDetailService;
import com.storecodes.rs.model.StoreDetail;
import com.storecodes.rs.util.ModelConverter;
import com.storecodes.service.StoreService;

public class StoreDetailServiceImpl implements StoreDetailService {

    @Autowired
    private StoreService service;

    public StoreDetailServiceImpl() {
    }

    @Override
    public Collection<StoreDetail> getStores(String searchQuery, double latitude, double longitude) {
        Collection<Store> stores = service.getStores(searchQuery, latitude, longitude);
        List<StoreDetail> storeDetails = ModelConverter.convertToStoreDetail(stores);

        return storeDetails;
    }

}
