package com.storecodes.rs.impl;

import java.util.Collection;
import java.util.List;

import javax.persistence.NoResultException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;

import com.storecodes.data.model.Code;
import com.storecodes.rs.api.StoreCodesService;
import com.storecodes.rs.model.AddCodeRequest;
import com.storecodes.rs.model.CodeDetail;
import com.storecodes.rs.model.StoreCode;
import com.storecodes.rs.util.ModelConverter;
import com.storecodes.service.StoreCodeServiceImpl;
import com.storecodes.service.model.AddStoreCode;

public class StoreCodesServiceImpl implements StoreCodesService {

    @Autowired
    private StoreCodeServiceImpl service;

    @Override
    public Collection<StoreCode> getCodesByGpsCoordinte(int accountId, double latitude, double longitude, int numberOfResults) {
        List<Code> result = service.getStoreCodes(accountId, latitude, longitude, numberOfResults);
        Collection<StoreCode> response = ModelConverter.convertToStoreCode(result);

        return response;
    }

    @Override
    public Collection<StoreCode> getStoreCodes(int accountId) {
        List<Code> result = service.getStoreCodes(accountId);
        Collection<StoreCode> response = ModelConverter.convertToStoreCode(result);

        return response;
    }

    @Override
    public int addStoreCode(int accountId, int customerId, AddCodeRequest code) {
        AddStoreCode newCode = ModelConverter.convertToAddStoreCode(customerId, code);

        return service.addStoreCode(newCode, accountId);
    }

    @Override
    public Collection<StoreCode> deleteStoreCode(UriInfo uriInfo, int customerId, int codeId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CodeDetail getDetail(int accountId, int codeId) {
        CodeDetail detail;

        try {
            Code storeCode = service.getStoreCode(accountId, codeId);
            detail = ModelConverter.convertToCodeDetail(storeCode);

        } catch (NoResultException exception) {
            throw new NotFoundException();
        }

        return detail;
    }
}
