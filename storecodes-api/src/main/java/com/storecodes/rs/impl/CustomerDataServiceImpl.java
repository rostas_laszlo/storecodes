package com.storecodes.rs.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.storecodes.data.model.Customer;
import com.storecodes.rs.api.CustomerDataService;
import com.storecodes.rs.model.CustomerInfo;
import com.storecodes.rs.util.ModelConverter;
import com.storecodes.service.CustomerService;

public class CustomerDataServiceImpl implements CustomerDataService {

    @Autowired
    private CustomerService customerService;

    public CustomerDataServiceImpl() {
    }

    @Override
    public Collection<CustomerInfo> getAffectedCustomers(int accountId) {
        List<Customer> customers = customerService.getAffectedCustomers(accountId);
        List<CustomerInfo> customerInfos = ModelConverter.convertToCustomerInfo(customers);

        return customerInfos;
    }
}
