package com.storecodes.rs.api;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.storecodes.rs.model.AddCodeRequest;
import com.storecodes.rs.model.CodeDetail;
import com.storecodes.rs.model.StoreCode;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

/**
 * This class serve to description of Store Codes serveces interface
 *
 */
@Path("/storecodes")
@Api(value = "/storecodes", description = "Manage Store Codes")
public interface StoreCodesService {

    @Produces({ MediaType.APPLICATION_JSON })
    @GET
    @ApiOperation(
        value = "list codes by coordinates",
        notes = "list codes by coordinates",
        response = StoreCode.class,
        responseContainer = "List"
    )
    Collection<StoreCode> getCodesByGpsCoordinte(
        @ApiParam(value = "accountId", required = true) @QueryParam("accountId") final int accountId,
        @ApiParam(value = "latitude", required = true) @QueryParam("latitude") final double latitude,
        @ApiParam(value = "longitude", required = true) @QueryParam("longitude") final double longitude,
        @ApiParam(value = "number of results", required = false) @QueryParam("numberOfResults") @DefaultValue("1") final int numberOfResults
    );

    @Produces({ MediaType.APPLICATION_JSON })
    @Path("/{accountId}")
    @GET
    @ApiOperation(
        value = "Account id to lookup for StoreCode",
        notes = "Account id to lookup for StoreCode",
        response = StoreCode.class,
        responseContainer = "List"
    )
    @ApiResponses({ @ApiResponse(code = 404, message = "Account id doesn't exists"), @ApiResponse(code = 204, message = "No result") })
    Collection<StoreCode> getStoreCodes(
        @ApiParam(value = "Account id to lookup for", required = true) @PathParam("accountId") final int accountId
    );

    @Produces({ MediaType.APPLICATION_JSON })
    @Path("/detail/{codeId}/{accountId}")
    @GET
    @ApiOperation(value = "Details of StoreCode", notes = "Details of StoreCode", response = CodeDetail.class)
    CodeDetail getDetail(
        @ApiParam(value = "accountId", required = true) @PathParam("accountId") final int accountId,
        @ApiParam(value = "codeId", required = true) @PathParam("codeId") final int codeId
    );

    /*
     * TODO error handling and responses
     */
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{customerId}/{accountId}")
    @POST
    @ApiOperation(value = "Account id to lookup for StoreCode", notes = "return with new id", response = Integer.class)
    @ApiResponses({ @ApiResponse(code = 404, message = "Account id doesn't exists") })
    int addStoreCode(
        @ApiParam(value = "Account id to lookup for", required = true) @PathParam("accountId") final int accountId,
        @ApiParam(value = "CustomerId id to lookup for", required = true) @PathParam("customerId") final int customerId,
        @ApiParam(value = "Account id to lookup for", required = true) final AddCodeRequest code
    );

    @Produces({ MediaType.APPLICATION_JSON })
    @Path("/{accountId}")
    @DELETE
    @ApiOperation(value = "Account id to lookup for StoreCode", notes = "Account id to lookup for StoreCode")
    @ApiResponses({ @ApiResponse(code = 404, message = "Account id doesn't exists") })
    Collection<StoreCode> deleteStoreCode(
        @Context final UriInfo uriInfo,
        @ApiParam(value = "Account id to lookup for", required = true) @PathParam("accountId") final int customerId,
        @ApiParam(value = "codeId", required = true) @QueryParam("codeId") final int codeId
    );

}
