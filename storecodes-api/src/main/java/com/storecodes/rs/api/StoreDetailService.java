package com.storecodes.rs.api;

import java.util.Collection;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.storecodes.rs.model.StoreDetail;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/store")
@Api(value = "/store", description = "Manage Stores")
public interface StoreDetailService {

    @Produces({ MediaType.APPLICATION_JSON })
    @GET
    @Path("/")
    @ApiOperation(value = "list stores", notes = "", response = StoreDetail.class, responseContainer = "List")
    Collection<StoreDetail> getStores(
        @ApiParam(value = "searchQuery", required = false) @QueryParam("searchQuery") final String searchQuery,
        @ApiParam(value = "latitude", required = false) @QueryParam("latitude") final double latitude,
        @ApiParam(value = "longitude", required = false) @QueryParam("longitude") final double longitude
    );
}
