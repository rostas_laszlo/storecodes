package com.storecodes.rs.api;

import java.util.Collection;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.storecodes.rs.model.CustomerInfo;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/customerData")
@Api(value = "/customerData", description = "Manage Customer Data")
public interface CustomerDataService {

    @Produces({ MediaType.APPLICATION_JSON })
    @GET
    @Path("/affectedCustomers/{accountId}")
    @ApiOperation(value = "list stores where the given user is affected.", notes = "", response = CustomerInfo.class, responseContainer = "List")
    Collection<CustomerInfo> getAffectedCustomers(
        @ApiParam(value = "Account id to lookup for", required = true) @PathParam("accountId") final int accountId
    );
}
