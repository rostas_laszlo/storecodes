package com.storecodes.rs.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
* Base Exception with rs response data.
* The rest response status is 404 NOT_FOUND
*
*/
public class NotFoundException extends WebApplicationException {
    private static final long serialVersionUID = -2894269137259898072L;

    public NotFoundException() {
        super(Response.status(Status.NOT_FOUND).build());
    }
}
