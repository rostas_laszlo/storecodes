package com.storecodes.rs.exception;

/**
* Specialized Exception with rs response data.
* This Exception for Account not found
*
*/
public class AccountNotFoundException extends NotFoundException {
    private static final long serialVersionUID = -2894269137259898072L;

    public AccountNotFoundException(final String email) {
    }
}
