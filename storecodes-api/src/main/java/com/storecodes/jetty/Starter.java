package com.storecodes.jetty;

import java.io.IOException;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.Resource;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import com.storecodes.spring.config.AppConfig;

public class Starter {
    private static final int SERVER_PORT = 8081;
    private static final String CONTEXT_PATH = "rest";

    public static void main(final String[] args) throws Exception {
        Resource.setDefaultUseCaches(false);

        System.setProperty(AbstractEnvironment.DEFAULT_PROFILES_PROPERTY_NAME, AppConfig.PRODUCTION_PROFILE_NAME);
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, AppConfig.PRODUCTION_PROFILE_NAME);
 
        System.setProperty(AppConfig.SERVER_PORT, Integer.toString(SERVER_PORT));
        System.setProperty(AppConfig.SERVER_HOST, "localhost");
        System.setProperty(AppConfig.CONTEXT_PATH, CONTEXT_PATH);

        final HandlerList handlers = getServletContextHandlers();

        final Server server = new Server(SERVER_PORT);
        server.setHandler(handlers);
        server.start();
        server.join();
    }

    private static HandlerList getServletContextHandlers() throws IOException {
        final ServletContextHandler context = getServletContextHandler();
        final ServletContextHandler swagger = getConfiguringSwaggerAsStaticWebResource();

        final HandlerList handlers = new HandlerList();
        handlers.addHandler(swagger);
        handlers.addHandler(context);
        return handlers;
    }

    private static ServletContextHandler getServletContextHandler() {
        final ServletHolder servletHolder = new ServletHolder(new CXFServlet());
        final ServletContextHandler context = new ServletContextHandler();
        context.setContextPath("/");
        context.addServlet(servletHolder, "/" + CONTEXT_PATH + "/*");
        context.addEventListener(new ContextLoaderListener());
        context.setInitParameter("contextClass", AnnotationConfigWebApplicationContext.class.getName());
        context.setInitParameter("contextConfigLocation", AppConfig.class.getName());
        return context;
    }

    private static ServletContextHandler getConfiguringSwaggerAsStaticWebResource() throws IOException {
        final ServletHolder swaggerHolder = new ServletHolder(new DefaultServlet());

        final ServletContextHandler swagger = new ServletContextHandler();
        swagger.setContextPath("/swagger");
        swagger.addServlet(swaggerHolder, "/*");
        swagger.setResourceBase(new ClassPathResource("/webapp").getURI().toString());

        return swagger;
    }
}
