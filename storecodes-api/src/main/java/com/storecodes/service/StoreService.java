package com.storecodes.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.storecodes.data.model.Store;
import com.storecodes.data.repositories.StoreDao;

/**
* Service class for stores data
*
*/
@Component
public class StoreService {

    @Autowired
    private StoreDao storeDao;
    
    public StoreService() {
    }

    /**
    * Return the nearby stores
    * @param searchQuery query. Free word search.
    * @param latitude gps latitude value of search node
    * @param longitude gps longitude value of search node
    * @return Stores which is fulfilled this conditionals
    *
    */
    public Collection<Store> getStores(String searchQuery, double latitude, double longitude) {
        List<Store> stores = storeDao.find(searchQuery, latitude, longitude);

        return stores;
    }
}
