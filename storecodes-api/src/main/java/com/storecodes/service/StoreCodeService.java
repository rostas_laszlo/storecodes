package com.storecodes.service;

import java.util.List;

import javax.persistence.LockTimeoutException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;

import com.storecodes.data.model.Code;
import com.storecodes.service.model.AddStoreCode;

public interface StoreCodeService {

    List<Code> getStoreCodes(int accountId);

    List<Code> getStoreCodes(int accountId, double latitude, double longitude, int numberOfResults);

    /**
     * Execute find query that returns a single Code result.
     *
     * @return the code result
     *
     * @throws NoResultException
     *             if there is no result
     * @throws NonUniqueResultException
     *             if more than one result
     * @throws IllegalStateException
     *             if called for a Java Persistence query language UPDATE or DELETE statement
     * @throws QueryTimeoutException
     *             if the query execution exceeds the query timeout value set and only the statement is rolled back
     * @throws TransactionRequiredException
     *             if a lock mode has been set and there is no transaction
     * @throws PessimisticLockException
     *             if pessimistic locking fails and the transaction is rolled back
     * @throws LockTimeoutException
     *             if pessimistic locking fails and only the statement is rolled back
     * @throws PersistenceException
     *             if the query execution exceeds the query timeout value set and the transaction is rolled back
     */
    Code getStoreCode(int accountId, int codeId);

    /**
     * Create new store code.
     * 
     * @return the new code id
     *
     * @throws NoResultException
     *             if there is no result
     * @throws NonUniqueResultException
     *             if more than one result
     * @throws IllegalStateException
     *             if called for a Java Persistence query language UPDATE or DELETE statement
     * @throws QueryTimeoutException
     *             if the query execution exceeds the query timeout value set and only the statement is rolled back
     * @throws TransactionRequiredException
     *             if a lock mode has been set and there is no transaction
     * @throws PessimisticLockException
     *             if pessimistic locking fails and the transaction is rolled back
     * @throws LockTimeoutException
     *             if pessimistic locking fails and only the statement is rolled back
     * @throws PersistenceException
     *             if the query execution exceeds the query timeout value set and the transaction is rolled back
     */
    int addStoreCode(AddStoreCode code, int accountId);
}
