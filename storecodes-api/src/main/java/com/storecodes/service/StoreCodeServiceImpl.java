package com.storecodes.service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.storecodes.data.model.Code;
import com.storecodes.data.model.RawCode;
import com.storecodes.data.repositories.CustomerDao;
import com.storecodes.data.repositories.StoreCodeDao;
import com.storecodes.data.repositories.StoreDao;
import com.storecodes.service.model.AddStoreCode;

@Component
public class StoreCodeServiceImpl implements StoreCodeService {

    @Autowired
    private StoreCodeDao storeCodeDao;

    @Autowired
    private CustomerDao customerDao;
    
    @Autowired
    private StoreDao storeDao;

    public StoreCodeServiceImpl() {
    }

    @Override
    public List<Code> getStoreCodes(int accountId) {
        List<Code> result = storeCodeDao.findAll(accountId);

        return result;
    }

    @Override
    public List<Code> getStoreCodes(int accountId, double latitude, double longitude, int numberOfResults) {
        List<Code> result = storeCodeDao.find(accountId, latitude, longitude, numberOfResults);

        return result;
    }

    @Override
    public Code getStoreCode(int accountId, int codeId) {
        return storeCodeDao.find(accountId, codeId);
    }

    @Override
    public int addStoreCode(AddStoreCode code, int accountId) {
        customerDao.find(accountId, code.getCustomerId());
        storeDao.find(code.getStoreId());

        RawCode rawCode = new RawCode();
        rawCode.setCreated(convertDate(code.getCreated()));
        rawCode.setCustomer(code.getCustomerId());
        rawCode.setData(code.getData());
        rawCode.setName(code.getName());
        rawCode.setStore(code.getStoreId());
        rawCode.setToValid(convertDate(code.getToValid()));
        rawCode.setType(code.getType());

        return storeCodeDao.persist(rawCode);
    }

    private Date convertDate(java.util.Date date) {
        return new java.sql.Date(date.getTime());
    }
}
