package com.storecodes.service.model;

import java.util.Date;

public class AddStoreCode {

    private int storeId;

    private String type;

    private String name;

    private String data;

    private Date toValid;

    private Date created;

    private int customerId;

    public AddStoreCode() {
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Date getToValid() {
        return toValid;
    }

    public void setToValid(Date toValid) {
        this.toValid = toValid;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}
