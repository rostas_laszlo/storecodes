package com.storecodes.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.storecodes.data.model.Customer;
import com.storecodes.data.repositories.CustomerDao;

/**
* Service class for customer data
*
*/
@Component
public class CustomerService {

    @Autowired
    private CustomerDao customerDao;

    public CustomerService() {
    }
    
    /**
    * @param accountId system account id
    * @return Affected all customers
    *
    */
    public List<Customer> getAffectedCustomers(int accountId) {
        return customerDao.getAffectedCustomers(accountId);
    }
}
