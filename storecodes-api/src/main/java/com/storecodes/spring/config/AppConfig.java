package com.storecodes.spring.config;

import java.util.Arrays;

import javax.ws.rs.ext.RuntimeDelegate;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.storecodes.rs.JaxRsApiApplication;
import com.storecodes.rs.api.CustomerDataService;
import com.storecodes.rs.api.StoreCodesService;
import com.storecodes.rs.api.StoreDetailService;
import com.storecodes.rs.impl.CustomerDataServiceImpl;
import com.storecodes.rs.impl.StoreCodesServiceImpl;
import com.storecodes.rs.impl.StoreDetailServiceImpl;
import com.storecodes.rs.model.StoreCode;
import com.wordnik.swagger.jaxrs.config.BeanConfig;
import com.wordnik.swagger.jaxrs.listing.ApiDeclarationProvider;
import com.wordnik.swagger.jaxrs.listing.ApiListingResourceJSON;
import com.wordnik.swagger.jaxrs.listing.ResourceListingProvider;

/**
* Spring configuration class.
* Define Spring beans for: cxf, swagger and service layer components
*
*/
@Configuration
@ComponentScan("com.storecodes")
@Profile(AppConfig.PRODUCTION_PROFILE_NAME)
public class AppConfig {
    public static final String PRODUCTION_PROFILE_NAME = "production";
    public static final String SERVER_PORT = "server.port";
    public static final String SERVER_HOST = "server.host";
    public static final String CONTEXT_PATH = "context.path";

    public AppConfig() {
    }

    @Bean(destroyMethod = "shutdown")
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    @DependsOn("cxf")
    public Server jaxRsServer() {
        JAXRSServerFactoryBean factory = RuntimeDelegate.getInstance().createEndpoint(jaxRsApiApplication(), JAXRSServerFactoryBean.class);
        factory.setServiceBeans(
            Arrays.<Object>asList(storeCodesService(), storeDetailService(), customerDataService(), apiListingResourceJson())
        );
        factory.setAddress(factory.getAddress());
        factory.setProviders(Arrays.<Object>asList(jsonProvider(), resourceListingProvider(), apiDeclarationProvider()));
        return factory.create();
    }

    @Bean
    @Autowired
    public BeanConfig swaggerConfig(org.springframework.core.env.Environment environment) {
        final BeanConfig config = new BeanConfig();
        config.setVersion("1.0.0");
        config.setScan(true);
        config.setResourcePackage(StoreCode.class.getPackage().getName());
        config.setBasePath(
            String.format(
                "http://%s:%s/%s%s",
                environment.getProperty(SERVER_HOST),
                environment.getProperty(SERVER_PORT),
                environment.getProperty(CONTEXT_PATH),
                jaxRsServer().getEndpoint().getEndpointInfo().getAddress()
            )
        );

        return config;
    }

    @Bean
    public ApiDeclarationProvider apiDeclarationProvider() {
        return new ApiDeclarationProvider();
    }

    @Bean
    public ApiListingResourceJSON apiListingResourceJson() {
        return new ApiListingResourceJSON();
    }

    @Bean
    public ResourceListingProvider resourceListingProvider() {
        return new ResourceListingProvider();
    }

    @Bean
    public JaxRsApiApplication jaxRsApiApplication() {
        return new JaxRsApiApplication();
    }

    @Bean
    public StoreCodesService storeCodesService() {
        return new StoreCodesServiceImpl();
    }

    @Bean
    public StoreDetailService storeDetailService() {
        return new StoreDetailServiceImpl();
    }

    @Bean
    public CustomerDataService customerDataService() {
        return new CustomerDataServiceImpl();
    }

    @Bean
    public JacksonJsonProvider jsonProvider() {
        return new JacksonJsonProvider();
    }
}
