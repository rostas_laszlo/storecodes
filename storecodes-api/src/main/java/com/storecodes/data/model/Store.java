package com.storecodes.data.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Store {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    private double longitudeGpsCoordinate;

    private double latitudeGpsCoordinate;

    private String address;

    public Store() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLongitudeGpsCoordinate() {
        return longitudeGpsCoordinate;
    }

    public void setLongitudeGpsCoordinate(double longitudeGpsCoordinate) {
        this.longitudeGpsCoordinate = longitudeGpsCoordinate;
    }

    public double getLatitudeGpsCoordinate() {
        return latitudeGpsCoordinate;
    }

    public void setLatitudeGpsCoordinate(double latitudeGpsCoordinate) {
        this.latitudeGpsCoordinate = latitudeGpsCoordinate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
