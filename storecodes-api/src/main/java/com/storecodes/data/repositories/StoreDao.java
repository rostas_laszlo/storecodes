package com.storecodes.data.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.storecodes.data.model.Store;

@Component
@Transactional
@Repository
public class StoreDao {

    @PersistenceContext
    private EntityManager entityManager;

    public StoreDao() {
    }

    @SuppressWarnings("unchecked")
    public List<Store> find(String searchQuery, double latitude, double longitude) {
        // TODO gps coordinate
        Query query = entityManager.createQuery("SELECT store FROM Store store WHERE UPPER(store.name) LIKE UPPER(:searchQuery)");
        query.setParameter("searchQuery", getSearcQueryString(searchQuery));

        return query.getResultList();
    }

    /**
     * Execute find query that returns a single Store result.
     *
     * @return the code result
     *
     * @throws NoResultException
     *             if there is no result
     * @throws NonUniqueResultException
     *             if more than one result
     * @throws IllegalStateException
     *             if called for a Java Persistence query language UPDATE or DELETE statement
     * @throws QueryTimeoutException
     *             if the query execution exceeds the query timeout value set and only the statement is rolled back
     * @throws TransactionRequiredException
     *             if a lock mode has been set and there is no transaction
     * @throws PessimisticLockException
     *             if pessimistic locking fails and the transaction is rolled back
     * @throws LockTimeoutException
     *             if pessimistic locking fails and only the statement is rolled back
     * @throws PersistenceException
     *             if the query execution exceeds the query timeout value set and the transaction is rolled back
     */
    public Store find(int storeId) {
        Query query = entityManager.createQuery("SELECT store FROM Store store WHERE store.id = :storeId");
        query.setParameter("storeId", storeId);

        return (Store) query.getSingleResult();
    }

    private String getSearcQueryString(String searchQuery) {
        String query = searchQuery == null ? searchQuery = "" : searchQuery;
        query = "%" + query + "%";

        return query;
    }
}
