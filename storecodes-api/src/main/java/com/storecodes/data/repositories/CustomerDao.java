package com.storecodes.data.repositories;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.storecodes.data.model.Customer;

/**
 * DAO service class for Customers data
 *
 */
@Component
@Transactional
@Repository
public class CustomerDao {

    @PersistenceContext
    private EntityManager entityManager;

    public CustomerDao() {
    }

    /**
     * @param customer
     *            new Customer data
     * @throws EntityExistsException
     *             if the customer already exists. (If the customer already exists, the
     *             <code>EntityExistsException</code> may be thrown when the persist operation is invoked, or the
     *             <code>EntityExistsException</code> or another <code>PersistenceException</code> may be thrown at
     *             flush or commit time.)
     * @throws IllegalArgumentException
     *             if the instance is not an customer
     * @throws TransactionRequiredException
     *             if invoked on a container-managed entity manager of type
     *             <code>PersistenceContextType.TRANSACTION</code> and there is no transaction
     *
     */
    public void persistCustomer(Customer customer) {
        entityManager.persist(customer);
    }

    /**
     * getAffectedCustomers
     * 
     * @param accountId
     *            system account id
     * @return a list of the customers
     *
     * @throws IllegalStateException
     *             if called for a Java Persistence query language UPDATE or DELETE statement
     * @throws QueryTimeoutException
     *             if the query execution exceeds the query timeout value set and only the statement is rolled back
     * @throws TransactionRequiredException
     *             if a lock mode has been set and there is no transaction
     * @throws PessimisticLockException
     *             if pessimistic locking fails and the transaction is rolled back
     * @throws LockTimeoutException
     *             if pessimistic locking fails and only the statement is rolled back
     * @throws PersistenceException
     *             if the query execution exceeds the query timeout value set and the transaction is rolled back
     *
     */
    @SuppressWarnings("unchecked")
    public List<Customer> getAffectedCustomers(int accountId) {
        Query query = entityManager
                .createQuery("SELECT customer FROM Customer customer JOIN customer.account account WHERE account.id = :accountId");
        query.setParameter("accountId", accountId);

        return query.getResultList();
    }

    /**
     * Find customer by accountId and customerId
     * 
     * @param accountId
     *            system account id
     * @param customerId
     *            customerId
     *
     * @return the Customer
     *
     * @throws NoResultException
     *             if there is no result
     * @throws NonUniqueResultException
     *             if more than one result
     * @throws IllegalStateException
     *             if called for a Java Persistence query language UPDATE or DELETE statement
     * @throws QueryTimeoutException
     *             if the query execution exceeds the query timeout value set and only the statement is rolled back
     * @throws TransactionRequiredException
     *             if a lock mode has been set and there is no transaction
     * @throws PessimisticLockException
     *             if pessimistic locking fails and the transaction is rolled back
     * @throws LockTimeoutException
     *             if pessimistic locking fails and only the statement is rolled back
     * @throws PersistenceException
     *             if the query execution exceeds the query timeout value set and the transaction is rolled back
     */
    public Customer find(int accountId, int customerId) {
        Query query = entityManager.createQuery(
            "SELECT customer FROM Customer customer JOIN customer.account account WHERE account.id = :accountId and customer.id = :customerId"
        );
        query.setParameter("accountId", accountId);
        query.setParameter("customerId", customerId);

        return (Customer) query.getSingleResult();
    }
}
