package com.storecodes.data.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockTimeoutException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.PessimisticLockException;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.storecodes.data.model.Code;
import com.storecodes.data.model.RawCode;

@Component
@Transactional
@Repository
public class StoreCodeDao {

    @PersistenceContext
    private EntityManager entityManager;

    public StoreCodeDao() {
    }

    public void persist(Code code) {
        entityManager.persist(code);
    }

    /**
     * Persist the new Code.
     * 
     * @return the new code inner ID * @throws EntityExistsException if the RawCode already exists. (If the RawCode
     *         already exists, the <code>EntityExistsException</code> may be thrown when the persist operation is
     *         invoked, or the <code>EntityExistsException</code> or another <code>PersistenceException</code> may be
     *         thrown at flush or commit time.)
     * @throws IllegalArgumentException
     *             if the instance is not an RawCode
     * @throws TransactionRequiredException
     *             if invoked on a container-managed entity manager of type
     *             <code>PersistenceContextType.TRANSACTION</code> and there is no transaction
     */

    public int persist(RawCode code) {
        entityManager.persist(code);
        entityManager.flush();

        return code.getId();
    }

    @SuppressWarnings("unchecked")
    public List<Code> find(int accountId, double latitude, double longitude, int numberOfResults) {
        Query query = entityManager.createQuery("SELECT p FROM Code p JOIN p.customer.account a WHERE a.id = :accountId");
        query.setParameter("accountId", accountId);

        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Code> findAll(int accountId) {
        Query query = entityManager.createQuery("SELECT p FROM Code p JOIN p.customer.account a WHERE a.id = :accountId");
        query.setParameter("accountId", accountId);

        return query.getResultList();
    }

    /**
     * Execute find query that returns a single Code result.
     *
     * @return the code result
     *
     * @throws NoResultException
     *             if there is no result
     * @throws NonUniqueResultException
     *             if more than one result
     * @throws IllegalStateException
     *             if called for a Java Persistence query language UPDATE or DELETE statement
     * @throws QueryTimeoutException
     *             if the query execution exceeds the query timeout value set and only the statement is rolled back
     * @throws TransactionRequiredException
     *             if a lock mode has been set and there is no transaction
     * @throws PessimisticLockException
     *             if pessimistic locking fails and the transaction is rolled back
     * @throws LockTimeoutException
     *             if pessimistic locking fails and only the statement is rolled back
     * @throws PersistenceException
     *             if the query execution exceeds the query timeout value set and the transaction is rolled back
     */
    public Code find(int accountId, int codeId) {
        Query query = entityManager.createQuery(
            "SELECT code FROM Code code JOIN code.customer.account account WHERE account.id = :accountId AND code.id = :codeId"
        );
        query.setParameter("accountId", accountId);
        query.setParameter("codeId", codeId);

        return (Code) query.getSingleResult();
    }
}
